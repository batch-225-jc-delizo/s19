// Conditional Statements - allows us to control the flow of our program.

// [SECTION] if, else if, else statement

/*
Syntax:

	if(condition){
		statement
	};

*/

let numA = 5;

if(numA > 3) {
	console.log('Hello');
}


// else if clause

let numB = 1;

if(numA > 3){
	console.log('Hello');
} else if (numB > 0) {
	console.log('World')
}

// --------------------------------

if (numA < 3){
	console.log('Hello');
} else if (numB > 0) {
	console.log('World');
}

// ================================

// else if in string

city = "Tokyo";

if (city === "New York"){
	console.log("Welcome to New York City!")
} else if (city === "Tokyo"){
	console.log("Welcome to Tokyo, Japan!")
}

// ================================

// else statement

let numC = -5;
let numD = 7;

if(numC > 0) {
	console.log('Hello');
} else if (numD === 0) {
	console.log('World');
} else {
	console.log('Again');
}

// if, else if and else statements with function

function determineTyphoonIntensity(windspeed) {

	if (windspeed < 30){
		return 'Not a typhoon yet.';
	}

	else if (windspeed <= 61) {
		return 'Tropical depression detected.';
	}
	else if (windspeed >= 62 && windspeed <= 88) {
		return 'Tropical storm detected.';
	}
	else if (windspeed >= 89 && windspeed <= 117) {
		return 'Severe Tropical Storm detected.';
	}
	else {
		return 'Typhoon detected.';
	}

}

let message = determineTyphoonIntensity(87);
console.log(message);

if (message == 'Tropical storm detected.') {
	console.warn(message)
}

// [SECTION] Conditional (Ternary) Operator
/*
	- The consitional (Ternary) Operator takes in three operands:
	1. condition
	2. expression to execute if the condition is truthy
	3. expression to execute if the condition is falsy

	Ternary operator is for shorthand code - Commonly used for single statement execution where the result consists of only one line of code

	-Syntax
		(expression) ? ifTrue : ifFalse;

		- Can be used as an alternative to an "if else" statement
*/

// Single Statement Execution
let ternaryResult = (1 < 18) ? true : false
console.log("Result of Ternary Operator: " + ternaryResult);


// Multiple Statement Execution
let name;

function isOfLegalAge() {
    name = 'John';
    return 'You are of the legal age limit';
}

function isUnderAge() {
    name = 'Jane';
    return 'You are under the age limit';
}

// The "parseInt" function converts the input receive into a number data type.

let age = parseInt(prompt("What is your age?"));
console.log(age);
let legalAge = (age >= 18) ? isOfLegalAge() : isUnderAge();
console.log("Result of Ternary Operator in functions: " + legalAge + ', ' + name);


// [SECTION] Switch Statement

/*
	- the switch statements evaluates an expression and matches the expression's value to a case clause. The switch will then execute the statements associated with that case, as well as statements in cases that follow the matching case.
	- Can be used an alternative to an "if, else if and else" statement where the data to be used in the condition is of an expected output.
	- The ".LowerCase()" function/method will change input received to from the prompt into lowercase letters ensuring a match with the switch case conditions if the user inputs are capitalized or uppercase letters.
*/

let day = prompt("What day of the week is it today?").toLowerCase();
console.log(day);

switch (day) {
    case 'monday': 
        console.log("The color of the day is red");
        break;
    case 'tuesday':
        console.log("The color of the day is orange");
        break;
    case 'wednesday':
        console.log("The color of the day is yellow");
        break;
    case 'thursday':
        console.log("The color of the day is green");
        break;
    case 'friday':
        console.log("The color of the day is blue");
        break;
    case 'saturday':
        console.log("The color of the day is indigo");
        break;
    case 'sunday':
        console.log("The color of the day is violet");
        break;
    default:
        console.log("Please input a valid day");
        break;
}